//
//  AddLocationViewController.swift
//  ArtursFirstApp
//
//  Created by Artūrs Rāviņš on 23/04/2019.
//  Copyright © 2019 students. All rights reserved.
//

import UIKit
import Firebase
import CodableFirebase

class AddLocationViewController: UIViewController {
    var refLocations: DatabaseReference!
    @IBOutlet weak var txtLatitude: UITextField!
    @IBOutlet weak var txtLongitude: UITextField!
    @IBOutlet weak var lblInfoMsg: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtDescription.layer.borderColor = UIColor.lightGray.cgColor
        self.txtDescription.layer.borderWidth = 1
        refLocations = Database.database().reference().child("LocationData")
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func btnAddItem(_ sender: UIButton) {
        addData()
    }
    
    func addData(){
        
        let mTextLatitude = Double(txtLatitude.text!) ?? 0
        let mTextLongitude = Double(txtLatitude.text!) ?? 0
        
        if mTextLatitude == 0 || mTextLongitude == 0 {
            let alert = UIAlertController(title: "Nav ievadīti nepieciešamie dati", message: "Lai Veiktu atzīmi kartē, nepieciešamas visas koordinātas ievadītas. ", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Labi", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            let key = refLocations.childByAutoId().key
            let newLocation = MapLocation(id: key, latitude: mTextLatitude, longitude: mTextLongitude, title: "added location", description: txtDescription.text! )
            let data = try! FirebaseEncoder().encode(newLocation)
            refLocations.child(key!).setValue(data) { (err, resp) in
                guard err == nil else {
                    self.lblInfoMsg.text = "Neizdevās"
                    print("Posting failed : ")
                    print(err!)
                    return
                }
                self.lblInfoMsg.text = "Pievienots"
                print(resp)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
