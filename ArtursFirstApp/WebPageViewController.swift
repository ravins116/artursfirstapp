//
//  WebViewController.swift
//  ArtursFirstApp
//
//  Created by Artūrs Rāviņš on 18/04/2019.
//  Copyright © 2019 students. All rights reserved.
//

import WebKit
import UIKit

class WebPageViewController: UIViewController, WKNavigationDelegate, WKUIDelegate  {

    @IBOutlet weak var myWeb: WKWebView!
    
    private let activityView = UIActivityIndicatorView(style: .gray)
    var mGithubFile: GithubFile!
    override func viewDidLoad() {
        super.viewDidLoad()
        myWeb.navigationDelegate = self

         let mLink:String = mGithubFile.html_url ?? ""
        self.myWeb.addSubview(activityView)
        activityView.center = self.view.center
        activityView.startAnimating()
        myWeb.navigationDelegate = self
        let request = URLRequest(url: URL(string: mLink)!)
        myWeb.load(request)
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    
        let css = ".Header-old, .reponav-wrapper, .breadcrumb , .clearfix { display: none } body { background-color : #fff} "
        let js = "var style = document.createElement('style'); style.innerHTML = '\(css)'; document.head.appendChild(style);"
        
        webView.evaluateJavaScript(js, completionHandler: nil)
        
        activityView.stopAnimating()
    }
}

