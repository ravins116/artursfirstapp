//
//  MapLocation.swift
//  ArtursFirstApp
//
//  Created by Artūrs Rāviņš on 23/04/2019.
//  Copyright © 2019 students. All rights reserved.
//

import Foundation

struct MapLocation : Codable {
    var id: String?
    var latitude: Double?
    var longitude: Double?
    var title: String?
    var description: String?
}
