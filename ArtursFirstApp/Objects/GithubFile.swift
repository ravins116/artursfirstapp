//
//  Files.swift
//  ArtursFirstApp
//
//  Created by Artūrs Rāviņš on 17/04/2019.
//  Copyright © 2019 students. All rights reserved.
//

import Foundation

struct GithubFile: Codable {
    var type: String?
    var size: Int?
    var name: String?
    var path: String?
    var html_url: String?
    var download_url: String?
}
