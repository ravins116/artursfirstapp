//
//  ImagePreviewViewController.swift
//  ArtursFirstApp
//
//  Created by Artūrs Rāviņš on 18/04/2019.
//  Copyright © 2019 students. All rights reserved.
//

import UIKit

class ImagePreviewViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var mImageView: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnDownloadFile: UIButton!
    
    var mGithubFile: GithubFile!
    var globalImage:UIImage? = nil
   
    override func viewDidLoad() {
        super.viewDidLoad()
        let mFileTitle:String = mGithubFile.name ?? ""
         let mFileSize:Int = mGithubFile.size ?? 0
        lblTitle.text = mFileTitle
        lblDescription.text =  "\(mFileTitle) faila izmērs ir \(mFileSize)"

        if let umImageURL = URL(string: mGithubFile.download_url ?? "") {// !!! placeholder url?
            NetworkLayer().downloadImage(from: umImageURL,
                                         onSuccess: { image in
                                            DispatchQueue.main.async{
                                                self.mImageView.image = image
                                                self.globalImage = image
                                            }
            })
        } else {
            self.mImageView.image = nil
        }
    }
    
    @IBAction func saveFile(_ sender: Any) {
        let imageData = self.mImageView.image!.pngData()
        if let compresedImage = UIImage(data: imageData!) {
            UIImageWriteToSavedPhotosAlbum(compresedImage, nil, nil, nil)
            
            let alert = UIAlertController(title: "Saglabāts", message: "Skati failu pie attēliem", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Labi", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Neizdevās", message: "Fails nav atrasts", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Labi", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
