//
//  GitlabFilesTableViewController.swift
//  ArtursFirstApp
//
//  Created by Artūrs Rāviņš on 17/04/2019.
//  Copyright © 2019 students. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON
import AVKit
import AVFoundation


class GitHubFilesTableViewController: UITableViewController {
    
    
    @IBOutlet var mTableView: UITableView!
    var bag = DisposeBag()
    var GithubFilesArray = [GithubFile]()
    private var GithubFilesTitleArray = [String]()
    var myIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mTableView.rowHeight = 100.0
        let mGitFile = NetworkLayer().loadGithubFile()
        print("mGitFile")
        print(mGitFile)
        mGitFile.asObservable()
            .skip(0)
            .map { $0 }
            .subscribe(onNext: {
                self.GithubFilesTitleArray.append($0.name!)
                self.GithubFilesArray.append($0)
            },
                       onCompleted:{
                        self.setUpFiles()
            }
            ).disposed(by: bag)
    
    }
    
    func setUpFiles () {
        DispatchQueue.main.async {
             self.mTableView.reloadData()
        }
    }
    
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.GithubFilesTitleArray.count
    }
    
   override  func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
  override  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath)
        let data = self.GithubFilesTitleArray[indexPath.row]
        print(data)
        cell.textLabel?.text = data
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        myIndex = indexPath.row
        print(myIndex)
        var fileType = 0 // 1 is video , 2 - .jpg
        let mfile = cell?.textLabel?.text ?? "Clicked"
        print(mfile)
        let myGithubFile: GithubFile = GithubFilesArray[myIndex]
        let tempUrl =  myGithubFile.download_url ?? ""

        if tempUrl.hasSuffix(".mp4") { // true
            print("Suffix exists")
            fileType = 1
        }
        if tempUrl.hasSuffix(".jpg") { // true
            print("Suffix exists")
            fileType = 2
        }
        
        switch fileType {
        case 2:
            performSegue(withIdentifier: "ImageInfo", sender: self)
        case 1:
            performSegue(withIdentifier: "VideoInfo", sender: self)
        default:
            performSegue(withIdentifier: "WebInfo", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Atpakaļ"
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
         let myGithubFile: GithubFile = GithubFilesArray[myIndex]
        if segue.identifier == "ImageInfo" {
            if let img_vc = segue.destination as? ImagePreviewViewController {
                img_vc.mGithubFile = myGithubFile
            }
        }
        if segue.identifier == "VideoInfo" {
            let destination = segue.destination as! AVPlayerViewController
            let url = URL(string: myGithubFile.download_url ?? "")
            destination.player = AVPlayer(playerItem: AVPlayerItem(url: url!))
            destination.player?.play()
        }
        if segue.identifier == "WebInfo" {
            if let web_vc = segue.destination as? WebPageViewController {
                web_vc.mGithubFile = myGithubFile
            }

        }
    }
}
