//
//  MapViewController.swift
//  ArtursFirstApp
//
//  Created by Students on 09/03/2019.
//  Copyright © 2019 students. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import CodableFirebase
import Foundation

class MapViewController: UIViewController, MKMapViewDelegate, UpdateMapOptionsDelegate {
    func updateMapOptions(mShowLocationsWithDescription: Bool, mShowLocationsBetweenMe: Bool) {
        showLocationsBetweenMe = mShowLocationsBetweenMe
        showLocationsWithDescription = mShowLocationsWithDescription
        
        if(showLocationsBetweenMe){
            UserDefaults.standard.set(true, forKey: "showLocationsBetweenMe")
        }else{
            UserDefaults.standard.removeObject(forKey: "showLocationsBetweenMe")
        }
        if(showLocationsWithDescription){
            UserDefaults.standard.set(true, forKey: "showLocationsWithDescription")
        }else{
            UserDefaults.standard.removeObject(forKey: "showLocationsWithDescription")
        }

    }
    
    var refLocations: DatabaseReference!
    let locationManager = CLLocationManager()
    @IBOutlet weak var myMapView: MKMapView!
    var showLocationsWithDescription =  UserDefaults.standard.bool(forKey: "showLocationsWithDescription")
    var showLocationsBetweenMe =  UserDefaults.standard.bool(forKey: "showLocationsBetweenMe")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refLocations = Database.database().reference().child("LocationData")
        
        myMapView.delegate = self
        myMapView.showsScale = true
        myMapView.showsPointsOfInterest = true
    }
    
    func startTrackingLocation() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            myMapView.showsUserLocation = true
        } else {
            startTrackingLocation()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
        
        Locations(onSuccess: {success in
            let mapLocations = success
                for mLocation in mapLocations {
                    let mLatitude = mLocation.latitude ?? 0
                    if mLatitude == 0 {
                        print(mLocation)
                    } else {
                       self.showLocationInMap(mLocation:mLocation)
                    }
                }
          })
    }
    
    func showLocationInMap(mLocation: MapLocation) -> Void {
        let mLongitude = mLocation.longitude ?? 0
         let mLatitude = mLocation.latitude ?? 0
        let mTitle = mLocation.title
        let mDescription = mLocation.description
        let point = MKPointAnnotation()
        point.coordinate =
            CLLocationCoordinate2D(latitude: mLatitude, longitude: mLongitude)
        point.title = mTitle
        point.subtitle = mDescription
        
        self.myMapView.addAnnotation(point)
    }
    
    func Locations(onSuccess success: @escaping (Array<MapLocation>) -> Void) {
        var itemsArray = [MapLocation]()
        refLocations.observeSingleEvent(of: .value, with: { (snapshot) in
           // print(snapshot.childrenCount)
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
                
                guard let mItem = rest.value as? [String: Any] else { continue }
                let mId = mItem["id"] as? String
                let mLatitude = mItem["latitude"] as? Double
                let mLongitude = mItem["longitude"] as? Double
                let mTitle = mItem["title"] as? String
                let mDescription = mItem["description"] as? String
                let mMapLocation = MapLocation(id: mId, latitude: mLatitude, longitude: mLongitude, title: mTitle, description: mDescription )
                
                self.myMapView.removeAnnotations(self.myMapView.annotations)

                let pointLatitude = mLatitude ?? 0
                if pointLatitude == 0 {
                    print(pointLatitude)
                } else {
                    let aLocation =  self.locationManager.location
                    let anotherLocation =   CLLocation.init(latitude: mLatitude!, longitude: mLongitude!)
                    let qdistance = aLocation!.distance(from: anotherLocation)

                    if self.showLocationsBetweenMe == true && qdistance / 1000 <= 10.0 {
                        
                        print("Tis item in 10 km")
                        if self.showLocationsWithDescription == true && mDescription != "" {
                            itemsArray.append(mMapLocation)
                            print("Tis item show in 10 km")
                        } else if self.showLocationsWithDescription == true && mDescription == ""
                        {
                            print("This item hidden in 10 km")
                        }
                        else if self.showLocationsWithDescription == false
                        {
                            print("This item show in 10 km")
                            itemsArray.append(mMapLocation)
                        }
                        else{
                            print("What is happen in 10 km?")
                            
                        }
                   
                    }
                    else if self.showLocationsBetweenMe == false {
                        
                        
                        if self.showLocationsWithDescription == true && mDescription != "" {
                            itemsArray.append(mMapLocation)
                            print("Tis item show")
                        } else if self.showLocationsWithDescription == true && mDescription == ""
                        {
                            print("This item hidden")
                        }
                        else if self.showLocationsWithDescription == false
                        {
                            print("This item show")
                            itemsArray.append(mMapLocation)
                        }
                        else{
                            print("What is happen?")
                            
                        }
                    }
                }
            }
            success(itemsArray)
        })
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.blue
        return renderer
    }
    

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if(annotation.title == "added location"){
            _ = annotation
        } else {
            return nil
        }

        let identifier = "marker"
        var view: MKMarkerAnnotationView
  
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            let mButton = UIButton(type: .detailDisclosure)
            view.rightCalloutAccessoryView = mButton
        }
        return view
    }
    

    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        self.myMapView.removeOverlays(self.myMapView.overlays)
        let destPoint = view.annotation

        let mySourceCoordinate = locationManager.location!.coordinate
        let destinationCoordinate = destPoint?.coordinate
        
        let sourcePacemark = MKPlacemark(coordinate: mySourceCoordinate)
        let destinationPacemark =  MKPlacemark(coordinate: destinationCoordinate!)
        
        let sourceItem = MKMapItem(placemark: sourcePacemark)
        let destItem = MKMapItem(placemark: destinationPacemark)
        
        let request = MKDirections.Request()
        request.source = sourceItem
        request.destination = destItem
        request.transportType = .automobile
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            for route in unwrappedResponse.routes {
                self.myMapView.addOverlay(route.polyline)
                self.myMapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Atpakaļ"
        navigationItem.backBarButtonItem = backItem // This will show in the next
        if segue.identifier == "MapOptions" {
        let dest = segue.destination as! MapOptionsViewController
        dest.delegate = self
        }
    }
}
