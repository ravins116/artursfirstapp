//
//  NetworkLayer.swift
//  ArtursFirstApp
//
//  Created by Artūrs Rāviņš on 17/04/2019.
//  Copyright © 2019 students. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON
class NetworkLayer{
private let url = URL (string: "https://api.github.com")
let imageCache = NSCache<AnyObject, AnyObject>()

//    https://api.github.com/users/karlis-berzins-accenture/repos
//    https://api.github.com/repos/karlis-berzins-accenture/via-test
//    https://api.github.com/repos/karlis-berzins-accenture/via-test/contents/
    
func loadGithubFile() -> Observable<GithubFile> {
    return Observable.create { mFile in
        let filesURL = self.url?.appendingPathComponent("repos/karlis-berzins-accenture/via-test/contents/")
        let task = URLSession.shared.dataTask(with: filesURL!){ [weak self] data, response, error in
            guard error == nil else {mFile.onError(error!); return}
            guard let data = data else { return}
            guard self != nil else {return}
            do{
                let filesJSON = try? JSON(data: data)
                
                
                for (_, itemJson): (String, JSON) in filesJSON! {
                    mFile.onNext( GithubFile(type: itemJson["type"].string,
                                          size: itemJson["size"].int,
                                          name: itemJson["name"].string,
                                          path: itemJson["path"].string,
                                          html_url: itemJson["html_url"].string,
                                          download_url: itemJson["download_url"].string))
                }
                
                
                mFile.onCompleted()
            }
        }
        task.resume()
        return Disposables.create {
            task.cancel()
        }
    }
}

    func downloadImage(from url: URL, onSuccess success: @escaping (UIImage) -> Void) {
        let keyURL = url as AnyObject
        //        if let value = imageDownloadTasks[url] {
        //            print("Will return \(value)")
        //            return
        //        }
        if let imageFromCache = imageCache.object(forKey: keyURL) as? UIImage {
            success(imageFromCache)
            return
        }
        let imageDownloadTask : URLSessionDataTask
        imageDownloadTask = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            guard let data = data,
                error == nil else {
                    return
            }
            //            if imageDownloadTasks[url] != nil {
            //            imageDownloadTasks.removeValue(forKey: url)
            //            }
            let image = UIImage(data: data)
            self.imageCache.setObject(image!, forKey: keyURL)
            //            imageDownloadTasks[indexPath] = imageDownloadTasks[indexPath]?.filter{$0 != imageDownloadTask}
            success(image!)
        })
        imageDownloadTask.resume()
        //imageDownloadTasks.updateValue({$0.append(imageDownloadTask)}, forKey: indexPath)
        
        return
    }

    
}
