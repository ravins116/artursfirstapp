//
//  MapOptionsViewController.swift
//  ArtursFirstApp
//
//  Created by Artūrs Rāviņš on 24/04/2019.
//  Copyright © 2019 students. All rights reserved.
//

import UIKit

protocol UpdateMapOptionsDelegate {
    func updateMapOptions(mShowLocationsWithDescription: Bool,
                          mShowLocationsBetweenMe: Bool)
}

class MapOptionsViewController: UIViewController {

    @IBOutlet weak var locationsWithDescription: UISwitch!
    @IBOutlet weak var locationsBetweenMe: UISwitch!
    var showLocationsWithDescription =  false
    var showLocationsBetweenMe =  false
    var delegate: UpdateMapOptionsDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func descriptionSwitchClicked(_ sender: UISwitch) {
        if sender.isOn {
            showLocationsWithDescription = true
        }
        else {
            showLocationsWithDescription = false
        }
    }
    
    @IBAction func betweenSwitchClicked(_ sender: UISwitch) {
        if sender.isOn {
            showLocationsBetweenMe = true
        }
        else {
            showLocationsBetweenMe = false
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            delegate?.updateMapOptions(mShowLocationsWithDescription: self.showLocationsWithDescription, mShowLocationsBetweenMe: self.showLocationsBetweenMe)
            dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showLocationsWithDescription =  UserDefaults.standard.bool(forKey: "showLocationsWithDescription")
        showLocationsBetweenMe =  UserDefaults.standard.bool(forKey: "showLocationsBetweenMe")
        if (showLocationsWithDescription){
            locationsWithDescription.isOn = true
        }
        if (showLocationsBetweenMe){
             locationsBetweenMe.isOn = true
        }
    }
}
