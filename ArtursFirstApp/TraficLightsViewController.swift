//
//  TraficLightsViewController.swift
//  ArtursFirstApp
//
//  Created by Artūrs Rāviņš on 13/04/2019.
//  Copyright © 2019 students. All rights reserved.
//

import UIKit

class TraficLightsViewController: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var viewLight: UIView!
    @IBOutlet weak var btnNow: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblTitle.text = "Ievadiet pārbaudāmā laika parametrus:"
        timePicker.setValue(UIColor.white, forKeyPath: "textColor")
        viewLight.layer.cornerRadius = viewLight.frame.size.width / 2
        viewLight.clipsToBounds = true
        viewLight.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        viewLight.layer.borderColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        viewLight.layer.borderWidth = 3.0
    }
    
    @IBAction func btnNowClick(_ sender: Any) {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)

        if(hour > 5){
            let mYear = "1/1/1971 "
            let mTime = "\(hour):\(minutes)"
            let mSeconds = ":00"
            let input = "\(mYear)\(mTime)\(mSeconds)"
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone.current
            formatter.dateFormat = "d-MM-yyyy HH:mm:ss"
            if let date = formatter.date(from: input) {
                print(date)  // Prints:  2018-12-10 06:00:00 +0000
                timePicker.setDate(date , animated: true)
            }
        } else {
            let alert = UIAlertController(title: "Luksofors nedarbojas", message: "Šobrīd nav aktīvās stundas. Izgulies un spied pa pogu. ", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Labi", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }

    }
    
    @IBAction func timeChanged(_ sender: Any) {
        timePicker.datePickerMode = .time
        let date = timePicker.date
        let components = Calendar.current.dateComponents([.hour, .minute], from: date)
        let hour = components.hour!
        var minute = components.minute!
        
        if(hour < 6 ){
            viewLight.isHidden = true
        } else {
            viewLight.isHidden = false
            let hourInMiutes = hour * 60
            minute = hourInMiutes + minute - (6*60)
            let tests = minute % 14

            switch tests {
            case 0...1:
                print("Number is between 0 & 1")
                viewLight.backgroundColor =  #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            case 2:
                print("Number is 2 ")
                viewLight.backgroundColor =  #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            case 3...5:
                print("Number is between 3 & 5")
                viewLight.backgroundColor =  #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
            case 6:
                print("Number is 6")
                viewLight.backgroundColor =  #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            case 7...8:
                print("Number is between 7 & 8")
                viewLight.backgroundColor =  #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            case 9:
                print("Number is 9")
                viewLight.backgroundColor =  #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            case 10...12:
                print("Number is between 10 & 12")
                viewLight.backgroundColor =  #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
            case 13:
                print("Number is 13")
                viewLight.backgroundColor =   #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            default:
                print("Fallback option")
                viewLight.backgroundColor =   #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            }
        }
        
    }
}
